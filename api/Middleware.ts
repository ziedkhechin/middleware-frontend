import {SERVER_URL} from "../constants/map";

const node_kafka = require("kafka-node");

export const fetchCloseMembers = (onSuccess: Function = () => null) => {
	const client = new node_kafka.KafkaClient({SERVER_URL}),
		topics = [{ topic: 'close-members', partition: 0 }],
		options = { autoCommit: false, fetchMaxWaitMs: 1000, fetchMaxBytes: 1024 * 1024 },
		consumer = new node_kafka.Consumer(client, topics, options);

	client.refreshMetadata(['close-members'], () => {
		consumer.on('message', (closeMembers: any[]) => {
			onSuccess(closeMembers);
		});
	});
};

export const sendCurrentPosition: Function = (closeMembers: any) => {
	const client = new node_kafka.KafkaClient({SERVER_URL});
	const producer = new node_kafka.Producer(client);

	producer.on('ready', () => {
		client.refreshMetadata(['close-members'], () => {
			producer.send(
				[{topic: 'close-members', messages: closeMembers}],
				() => process.exit()
			);
		})
	});
};
