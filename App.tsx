import React from 'react';
import "react-native-gesture-handler";
import TrackingMap from "./appComponents/screens/TrackingMap";
import {Provider} from "react-redux";
import {Store} from "./appComponents/redux/store";
import {SafeAreaProvider} from "react-native-safe-area-context/src/SafeAreaContext";
import {LogBox} from "react-native";

export default function App() {
    LogBox.ignoreAllLogs(true);

    return (
    <SafeAreaProvider>
        <Provider store={Store}>
            <TrackingMap />
        </Provider>
    </SafeAreaProvider>
    );
}